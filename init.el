(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

(require 'use-package)

(use-package flycheck
  :ensure t
  :init (flycheck-mode))

(use-package company
  :ensure t
  :init (global-company-mode))

(defun cpp-config ()
  (use-package irony
    :ensure t)

  (irony-mode)

  (semantic-mode)

  (setq
   ;; use gdb-many-windows by default
   gdb-many-windows t
   ;; Non-nil means display source file containing the main routine at startup
   gdb-show-main t)


  (use-package clang-format
    :ensure t
    :config
    (let* ((style "LLVM"))
      (setq clang-format-style (upcase style))
      (setq c-default-style style)))

  (use-package irony-eldoc
    :ensure t
    :init (irony-eldoc))


  (use-package company-irony
    :ensure t
    :after (irony company)
    :config (add-to-list 'company-backends 'company-irony))

  (use-package company-c-headers
    :ensure t
    :config
    (add-to-list 'company-c-headers-path-system "/usr/include/c++/8/")
    (add-to-list 'company-backends 'company-c-headers)))

(defun user/generate-tags ()
  "Generate TAG file for C/C++ projects"
  (interactive)
  (let* ((project-root (projectile-project-root)))
    (shell-command
     (concat "cd " 
	     (shell-quote-argument project-root)  
	     " && ctags-universal -h \".h.hpp.cpp.c\" -e -R " (shell-quote-argument project-root)))))




(add-hook 'c++-mode-hook 'cpp-config)
(add-hook 'c-mode-hook 'cpp-config)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(company-irony irony-eldoc clang-format irony company-c-headers use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
